/**
 * Each new term in the Fibonacci sequence is generated by adding the previous two terms. 
 * By starting with 1 and 2, the first 10 terms will be:
 * 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, ...
 * By considering the terms in the Fibonacci sequence whose values do not exceed four million, 
 * find the sum of the even-valued terms.
 */

 export const getSumOfEvenValuedTermsInFibonacciSequence = maxValueInSequence => {
    
    let total = 0
    let lastValue = 1
    let next = 1
    while(lastValue < maxValueInSequence) {

        if(lastValue % 2 === 0) {
            total += lastValue
        }

        if(lastValue === 1) {
            lastValue = next
            next++
        } else {
            let tmpNext = next
            let tempLast = lastValue
            next = lastValue + next
            lastValue = tmpNext 
        }
    }
    return total
 }