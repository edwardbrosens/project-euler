import { getMultiplesOfNumebersBelowNumber, sumOfElements } from './euler_1'

test('Is should get multiples of numbers below a number', () => {

    const numbers = [3]
    const belowNumber = 10
     
    expect(getMultiplesOfNumebersBelowNumber(numbers, belowNumber)).toEqual([0,3,6,9])
})

test('Is should sum all elements in an array', () => {

    const numbers = [0,3,6,9]
     
    expect(sumOfElements(numbers)).toBe(18)
})

test('Euler 1 solution should be', () => {
    const numbers = [3,5]
    const belowNumber = 1000
    
    const multiples = getMultiplesOfNumebersBelowNumber(numbers, belowNumber)
    const sum = sumOfElements(multiples)

    expect(sum).toBe(233168)

})