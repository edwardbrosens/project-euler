/**
 * Multiples of 3 and 5
 * 
 * If we list all the natural numbers below 10 that are multiples of 3 or 5, 
 * we get 3, 5, 6 and 9. The sum of these multiples is 23.
 * Find the sum of all the multiples of 3 or 5 below 1000.
 */

 export const getMultiplesOfNumebersBelowNumber = (numbers, belowNumber) => {

    let belowNumberArray = [...Array(belowNumber).keys()]
    return belowNumberArray.filter(belowNumberToMatch => {
        return numbers.filter(number => belowNumberToMatch % number === 0).length > 0
    })
 }

 export const sumOfElements = elements => elements.reduce(add, 0)

 export const add = (a, b) => a + b