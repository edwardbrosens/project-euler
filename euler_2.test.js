import { getSumOfEvenValuedTermsInFibonacciSequence } from './euler_2'

test('Is should get the sum of even numbers in a fibonacci sequence', () => {
    const maxValue = 10 
    // 1 [2] 3 5 [8] 
    // 2 + 8 = 10
    expect(getSumOfEvenValuedTermsInFibonacciSequence(maxValue)).toBe(10)
})


test('Is should get the sum of even numbers in a fibonacci sequence', () => {
    const maxValue = 4000000 
    expect(getSumOfEvenValuedTermsInFibonacciSequence(maxValue)).toBe(4613732)
})